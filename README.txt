LDAP SimpleOAuth integration

This module is a bridge between:

- the Simple OAuth module, which allows decoupled authentication (but only for local Drupal users), and
- the LDAP module, which allows users from an LDAP server to login, but only through the standard login form

Simply install this module as per normal, and instead of using local users, you will be using LDAP to perform authentication. This has the (likely desirable) side effect that policies configured in LDAP (eg allowing mixed mode login) are inherited by this module (since the actual credential validation is done by the LDAP module).

Under the hood, this module simply replaces the UserRepository service used by the Simple OAuth module with its own, which uses LDAP's LoginValidator service instead of cores' UserAuth service
