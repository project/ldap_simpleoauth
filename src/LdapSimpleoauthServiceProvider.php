<?php

namespace Drupal\ldap_simpleoauth;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

class LdapSimpleoauthServiceProvider extends ServiceProviderBase {
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('simple_oauth.repositories.user');
    $definition->setClass('Drupal\ldap_simpleoauth\LdapSimpleoauthUserRepository')
      ->setArguments([
        new Reference('config.factory'),
        new Reference('ldap_authentication.login_validator'),
        new Reference('user.auth')
      ]);
  }
}
