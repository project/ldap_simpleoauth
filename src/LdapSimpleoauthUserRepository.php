<?php

namespace Drupal\ldap_simpleoauth;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

use Drupal\Core\Form\FormState;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\user\UserAuthInterface;

use Drupal\simple_oauth\Entities\UserEntity;

use Drupal\ldap_authentication\Helper\LdapAuthenticationConfiguration;
use Drupal\ldap_authentication\Controller\LoginValidator;
use Drupal\ldap_servers\Helper\CredentialsStorage;


class LdapSimpleoauthUserRepository implements UserRepositoryInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config_factory;

  /**
   * @var \Drupal\ldap_authentication\Controller\LoginValidatorLoginForm
   */
  protected $ldap_validator;

  /**
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * UserRepository constructor.
   *
   * @param \Drupal\ldap_authentication\Controller\LoginValidator
   *   The LDAP service to check the user authentication.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoginValidator $ldap_validator, UserAuthInterface $user_auth) {
    $this->config_factory = $config_factory;
    $this->ldap_validator = $ldap_validator;
    $this->userAuth       = $user_auth;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserEntityByUserCredentials($username, $password, $grantType, ClientEntityInterface $clientEntity) {
    $config = $this->config_factory->get('ldap_authentication.settings');

    switch($config->get('authenticationMode')) {
      case LdapAuthenticationConfiguration::MODE_MIXED:
      // Mixed mode, try local auth first
        if ($uid = $this->userAuth->authenticate($username, $password)) {
           return $this->makeUseObject($uid);
        }

      // Intentional fallthrough
      case LdapAuthenticationConfiguration::MODE_EXCLUSIVE:
        // First build the formstate to imitate a login form
        $state = new FormState();
        $state->setValue('name', $username);
        $state->setValue('pass', $password);

        // We shouldn't need this
        CredentialsStorage::storeUserPassword($password);

        // Call LDAP Validator to check credential validity.
        //NOTE: This will also (potentially) check local accounts as well, as per LDAP configuration.
        $state = $this->ldap_validator->validateLogin($state);

        if($state->get('uid') != null) {
          // SimpleOAuth expects a simple user object, not a fully loaded user.
          return $this->makeUseObject($state->get('uid'));
        }
    }


    // Error out if it doesn't work out.
    throw OAuthServerException::invalidCredentials();
  }

  private function makeUseObject($uid) {
    $user = new UserEntity();
    $user->setIdentifier($uid);
    return $user;
  }
}
